LINKER_FILE = f'{PRO_DIR}/../env_{COMPILER}/linker/ThoNV_stm32f407.icf'
LK_FILE = f'{COMPILER_DIR_IAR}/bin/ilinkarm.exe'
CC_FILE = f'{COMPILER_DIR_IAR}/bin/iccarm.exe'
AC_FILE = f'{COMPILER_DIR_IAR}/bin/iasmarm.exe'
POST_COMPILE = f'{COMPILER_DIR_IAR}/bin/ielftool.exe'

BIN_COMPILE_OPTS= [
    '--bin',
]

HEX_COMPILE_OPTS = [
    '--ihex'
]

CC_OPTS = [
    '-c',
    '--cpu=Cortex-M4',
    '--cpu_mode=thumb',
    '--endian=little',
    '--debug',
    '-Ohz',
    '--no_mem_idioms',
    '--diag_suppress=Pa050',
    '-DIAR',
    '--require_prototypes',
    '--no_wrap_diagnostics',
    '-e'
]

LK_OPTS = [
    '--cpu=Cortex-M4',
    '--entry=Reset_Handler',
    '--config',
    f'{LINKER_FILE}',
    '--map',
    'MAP_FILE_REPLACE',
    '--skip_dynamic_initialization',
    '--no_wrap_diagnostics'
]
AC_OPTS = [
    '-r',
]